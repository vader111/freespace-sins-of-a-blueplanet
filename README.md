# README #

Welcome To the Open Alpha Version of FreeSpace Sins Of A Blueplanet

### What is this repository for? ###

* Open Alpha Version Of SOASE BP
* 0.4

### How do I get set up? ###

* Remember This is a Open Alpha, So anything you see will or might change, and Prepare for bugs.

* Dependencies: Sins of a Solar Empire Rebellion v1.91, A Decent PC
* How to run tests: Just Play the mod and give feedback
* Deployment instructions: Download the latest master zip and extract to your your sins mods folder and activate the mod in sins

### Contribution guidelines ###

* Writing tests: For the best way of getting issues fixed, submit a issue on the issues board and give accurate descriptions of what exactly happened, I also might ask you to create a debug log file for me to look at.

* Other guidelines: Again Remember this is a Open Alpha So things might not work as described and things will or might change or the course of development.

### Who do I talk to? ###

* Repo owner or admin